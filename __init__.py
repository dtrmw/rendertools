bl_info = {
	"name": "Render Tools",
	"author": "Dharma, DTRMediaWorks",
	"version": (0, 1),
	"blender": (2, 79, 0),
	"description": "Render tools",
	"category": "DTR MediaWorks"
}

import os
import json

import bpy
from bpy.props import (
	StringProperty,
	IntProperty,
	FloatVectorProperty,
	CollectionProperty,
	BoolProperty,
	EnumProperty
)
from bpy.types import (
	Operator,
	Panel,
	PropertyGroup
)

def clearNodes(nodeTree):
	for node in nodeTree.nodes:
		nodeTree.nodes.remove(node)


class RenderOutputSetup(Operator):
	bl_idname = "rendertools.setup"
	bl_label = "Setup Output Nodes"
	bl_options = {'REGISTER'}

	def execute(self, context):
		context.scene.use_nodes = True
		renderLayers = context.scene.render.layers
		compNodeTree = context.scene.node_tree
		nodeWidth = 300
		if context.scene.clear_existing_node_setup:
			clearNodes(compNodeTree)

		for rlIndex in range(len(renderLayers)):
			renderLayer = renderLayers[rlIndex]
			rlNode = compNodeTree.nodes.new("CompositorNodeRLayers")
			rlNode.name = "RL_" + renderLayer.name
			rlNode.label = "RL_" + renderLayer.name
			rlNode.location = (2 * nodeWidth * rlIndex, 0.0)
			rlNode.layer = renderLayer.name

			rlOutNode = compNodeTree.nodes.new("CompositorNodeOutputFile")
			rlOutNode.name = "RLOUT_" + renderLayer.name
			rlOutNode.label = "RLOUT_" + renderLayer.name
			rlOutNode.location = (2 * nodeWidth * rlIndex + nodeWidth, 0.0)
			rlOutNode.inputs.clear()
			rlOutNode.base_path = os.path.join(
				bpy.context.scene.render.filepath, 
				renderLayer.name
			)

			#ToDo : Find a way to optimize following line
			if rlOutNode.format.file_format == "OPEN_EXR_MULTILAYER":
				rlOutNode.base_path = os.path.join(
					rlOutNode.base_path,
					renderLayer.name
				)

			for rlNodeOutput in  rlNode.outputs:
				if not rlNodeOutput.enabled or rlNodeOutput.name == "Alpha":
					continue

				inputSlot = None
				if rlOutNode.format.file_format == "OPEN_EXR_MULTILAYER":
					inputSlot = rlOutNode.file_slots.new(rlNodeOutput.name)
				else :
					inputSlot = rlOutNode.file_slots.new("{1}/{1}_{0}".format(
							renderLayer.name, 
							rlNodeOutput.name
						)
					)
				compNodeTree.links.new(rlNodeOutput, inputSlot)
			
		return {"FINISHED"}


class LoadRenderImages(Operator):
	bl_idname = "rendertools.loadlayers"
	bl_label = "Load Rendered EXR Images"
	bl_options = {'REGISTER'}


	def execute(self, context):
		# ToDo : Support PNG. Now it's just EXR
		# file path is take from output path.
		context.scene.use_nodes = True
		compNodeTree = context.scene.node_tree
		nodeWidth = 400
		nodeHeight = 500
		if context.scene.clear_existing_node_setup:
			clearNodes(compNodeTree)

		renderFilesFolder = context.scene.render.filepath
		# changing output path to avoid overwriting of data
		context.scene.render.filepath = "/tmp/"

		filesInFolder = os.listdir(renderFilesFolder)
		isFirstNode = True
		isBGFound = False
		nodeCount = 0
		prevNode = None

		for fileInFolder in filesInFolder:
			filePath = os.path.join(renderFilesFolder, fileInFolder)
			if not os.path.isdir(filePath):
				continue

			imageFiles = os.listdir(filePath)
			if not imageFiles:
				continue
				
			if fileInFolder == "BG":
				isBGFound = True
				continue

			#Will assume it's a render layer images folder
			# Following cde is moved to a function.
			# ToDo : Improve that function
			'''
			imageNode = compNodeTree.nodes.new('CompositorNodeImage')
			imageData = bpy.data.images.load(os.path.join(filePath, imageFiles[0]), True)
			imageNode.image = imageData
			imageNode.image.source = "SEQUENCE"
			if isFirstNode:
				imageNode.location = (0,0)
				isFirstNode = False
				prevNode = imageNode
				continue

			imageNode.location = (nodeCount * nodeWidth, nodeHeight)
			alphaOverNode = compNodeTree.nodes.new('CompositorNodeAlphaOver')
			nodeCount = nodeCount + 1
			alphaOverNode.location = (nodeCount * nodeWidth,0)
			compNodeTree.links.new(imageNode.outputs["Image"], alphaOverNode.inputs[1])
			compNodeTree.links.new(prevNode.outputs["Image"], alphaOverNode.inputs[2])
			prevNode = alphaOverNode
			'''
			
			isFirstNode, prevNode, nodeCount = self.crateAndConnectNodes(
				os.path.join(filePath, imageFiles[0]),
				compNodeTree, 
				prevNode,
				nodeCount, 
				isFirstNode,
				nodeWidth,
				nodeHeight
			)

		if isBGFound:
			bgFilePath = filePath = os.path.join(renderFilesFolder, "BG")
			bgFiles = os.listdir(bgFilePath)
			if bgFiles and bgFiles[0]:
				isFirstNode, prevNode, nodeCount = self.crateAndConnectNodes(
					os.path.join(bgFilePath, bgFiles[0]), 
					compNodeTree, 
					prevNode, 
					nodeCount, 
					isFirstNode,
					nodeWidth,
					nodeHeight
				)
			
		return {"FINISHED"}

	# ToDo : Improve. Too many parameters are taken as of now
	def crateAndConnectNodes(self, fileToLoad, compNodeTree, prevNode, nodeCount, isFirstNode, nodeWidth, nodeHeight):
		imageNode = compNodeTree.nodes.new('CompositorNodeImage')
		imageName = os.path.basename(fileToLoad)
		# imageData = bpy.data.images.load(fileToLoad, True)
		bpy.ops.image.open(filepath=fileToLoad, 
			files=[{"name":imageName, "name":imageName}], 
			relative_path=True, 
			show_multiview=False)
		imageNode.image = bpy.data.images[imageName]
		imageNode.image.source = "SEQUENCE"
		if isFirstNode:
			imageNode.location = (0,0)
			return False, imageNode, nodeCount

		imageNode.location = (nodeCount * nodeWidth, nodeHeight)
		alphaOverNode = compNodeTree.nodes.new('CompositorNodeAlphaOver')
		nodeCount = nodeCount + 1
		alphaOverNode.location = ((nodeCount) * nodeWidth,0)
		compNodeTree.links.new(imageNode.outputs["Image"], alphaOverNode.inputs[1])
		compNodeTree.links.new(prevNode.outputs["Image"], alphaOverNode.inputs[2])
		# //ToDo Imrpove - Returned false so tha isFirstNode will become false after first call
		return False, alphaOverNode, nodeCount


class RenderLayersToInclude(PropertyGroup):
	layerName = StringProperty()
	isIncluded = BoolProperty()

class RenderLayersListItems(bpy.types.UIList):
	"""docstring for ListItems"""
	def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
		# column = layout.column()
		row = layout.row()
		row.prop(item, "isIncluded", text="")
		row.label(item["layerName"])

class PrepareRenderQueueJSON(Operator):
	bl_idname = "rendertools.prepare_json"
	bl_label = "Prepare JSON Config"
	bl_options = {'REGISTER'}

	#ToDo : Find a way to remove hard coded defaults
	srcFile = StringProperty(name="File To Render")
	renderPath = StringProperty(name="Render Path")
	renderFrames = StringProperty(name="Render Frames", default="101")
	renderResolution = IntProperty(name="Render Resolution", default=50, min=0, max=100, subtype="PERCENTAGE")
	renderSamples = IntProperty(name="Render Samples", default=128)
	renderWith = EnumProperty(name="Compute Device", items=[
		("GPU", "GPU", "", 1),
		("CPU", "CPU", "", 2)
	])
	renderFormat = EnumProperty(name="File Format", items=[
		("OPEN_EXR_MULTILAYER", "OPEN_EXR_MULTILAYER", "", 1),
		("PNG", "PNG", "", 2)
	])
	layersToInclude = CollectionProperty(type=RenderLayersToInclude)
	selectedLayerIndex = IntProperty(default=1)
	saveName = StringProperty(name="Save As", default="")
	renderThreads = IntProperty(name="No of Threads",default=-1)

	def invoke(self, context, event):
		rLayers = bpy.context.scene.render.layers
		self.layersToInclude.clear()
		for layer in rLayers:
			layerData = self.layersToInclude.add()
			layerData.layerName = layer.name
			layerData.isIncluded = layer.use
		self.srcFile = bpy.data.filepath
		return context.window_manager.invoke_props_dialog(self, width=800)

	def draw(self, context):
		layout = self.layout
		#ToDo : remove Assetizer dependancy if possible
		#Remove hard coded values
		assetInfo = context.scene.asset_info
		self.renderPath = "E:/Shows/CircleOfLife/renders/FinalRenders/{0}/{1}/".format(assetInfo.sequenceName, assetInfo.assetName )
		self.renderFrames = "{0} {1}".format(101, 100 + assetInfo.shotLength)
		self.saveName = "E:/Software/rqManagerCLI/workspace/{0}_rqItem.json".format(assetInfo.assetName)
		layout.prop(self, "srcFile")
		layout.prop(self, "renderPath")
		layout.template_list("RenderLayersListItems",
			"",
			self,
			"layersToInclude",
			self,
			"selectedLayerIndex"
		)
		layout.prop(self, "renderFrames")
		layout.prop(self, "renderResolution")
		layout.prop(self, "renderSamples")
		layout.prop(self, "renderWith")
		layout.prop(self, "renderFormat")
		layout.prop(self, "renderThreads")
		layout.prop(self, "saveName")

	def execute(self, context):
		renderData = {
			"src" : self.srcFile,
			"frames": self.renderFrames,
			"output_path":self.renderPath,
			"samples": self.renderSamples, 
			"device": self.renderWith, 
			"resolution": self.renderResolution, 
			"renderFormat": self.renderFormat,
			"threads" : self.renderThreads,
			"renderLayers" : self.getSelectedRenderLayersList()
		}

		with open(self.saveName, "w") as dataFile:
			json.dump(renderData, dataFile, indent=2)

		print ("====>{0}".format(renderData))
		return {"FINISHED"}

	def getSelectedRenderLayersList(self):
		retList = []
		for eachRLayerItem in self.layersToInclude:
			if eachRLayerItem.isIncluded:
				retList.append(eachRLayerItem.layerName)
		return retList


class RenderToolsPanel(Panel):
	bl_idname = "rendertools.panel"
	bl_space_type = "PROPERTIES"
	bl_region_type = "WINDOW"
	bl_context = "render"
	bl_label = "Render Tools"
	bl_description = ""
	bl_category = "DTR MediaWorks"

	def draw(self, context):
		layout = self.layout
		layout.prop(context.scene, "clear_existing_node_setup")
		layout.operator(RenderOutputSetup.bl_idname)
		layout.operator(LoadRenderImages.bl_idname)
		layout.operator(PrepareRenderQueueJSON.bl_idname)


def register():
	bpy.utils.register_class(RenderToolsPanel)
	bpy.utils.register_class(RenderOutputSetup)
	bpy.utils.register_class(LoadRenderImages)
	bpy.utils.register_class(RenderLayersToInclude)
	bpy.utils.register_class(RenderLayersListItems)
	bpy.utils.register_class(PrepareRenderQueueJSON)
	bpy.types.Scene.clear_existing_node_setup = BoolProperty(name="Clear Existing Node Setup", default=True)


def unregister():
	bpy.utils.unregister_class(RenderToolsPanel)
	bpy.utils.unregister_class(RenderOutputSetup)
	bpy.utils.unregister_class(LoadRenderImages)
	bpy.utils.unregister_class(RenderLayersToInclude)
	bpy.utils.unregister_class(RenderLayersListItems)
	bpy.utils.unregister_class(PrepareRenderQueueJSON)
	del bpy.types.Scene.clear_existing_node_setup


def main():
	register()

if __name__ == '__main__':
	main()